package org.coderfun.fieldmeta.service;

import klg.j2ee.common.dataaccess.BaseService;
import org.coderfun.fieldmeta.entity.EntityField;

public interface EntityFieldService extends BaseService<EntityField, Long>{

}
