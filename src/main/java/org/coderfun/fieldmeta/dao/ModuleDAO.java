package org.coderfun.fieldmeta.dao;

import klg.j2ee.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.Module;

public interface ModuleDAO extends BaseRepository<Module, Long> {

}
