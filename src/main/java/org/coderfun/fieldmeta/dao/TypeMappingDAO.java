package org.coderfun.fieldmeta.dao;

import klg.j2ee.common.dataaccess.BaseRepository;
import org.coderfun.fieldmeta.entity.TypeMapping;

public interface TypeMappingDAO extends BaseRepository<TypeMapping, Long> {

}
